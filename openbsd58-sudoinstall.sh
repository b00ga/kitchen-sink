#!/bin/sh
echo "Installing sudo..."
export PKG_PATH=http://mirrors.mit.edu/pub/OpenBSD/$(uname -r)/packages/$(machine -a)/
doas pkg_add -v sudo-1.8.14.3
doas sed -i 's/^# %wheel\(.*NOPASSWD\)/%wheel\1/' /etc/sudoers
grep wheel /etc/sudoers
