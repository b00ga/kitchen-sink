#!/bin/sh -x

#ls -l /etc/*release
#cat /etc/*release
#uname -a
#which curl
#which wget
#which git
#which unzip
#which 7z
#which 7za
#which tar
#which gzip
#which bzip2
#which xz

set -eux

RUBY_BUILD_REPO="https://github.com/rbenv/ruby-build.git"
RUBY_VERSION="2.2.4"

INSTALL_CMD=""

# http://stackoverflow.com/a/20990596
if [ -x /usr/bin/dnf -o -x /usr/bin/yum ]; then
   # Get "releasever" as suggested here http://stackoverflow.com/questions/20988371/linux-bash-get-releasever-and-basearch-values/20990596#20990596
   ###distro=$(sed -n 's/^distroverpkg=//p' /etc/yum.conf)
   ###releasever=$(rpm -q --qf "%{version}" -f /etc/$distro)
   releasever=$(rpm -q --qf "%{version}" -f /etc/redhat-release)
   if [ "${releasever}" = "5" ]; then
      # Install EPEL needed for git
      /usr/bin/wget --no-check-certificate -O/var/tmp217521F6.txt https://getfedora.org/static/217521F6.txt
      /bin/rpm --import /var/tmp217521F6.txt
      /usr/bin/wget -O/var/tmp/epel-release-latest-5.noarch.rpm https://dl.fedoraproject.org/pub/epel/epel-release-latest-5.noarch.rpm
      /usr/bin/yum install -y /var/tmp/epel-release-latest-5.noarch.rpm
   fi
   if [ -x /usr/bin/dnf ]; then
      INSTALL_CMD="/usr/bin/dnf install -y"
      /usr/bin/dnf groupinstall -y "Development Tools"
   else
      INSTALL_CMD="/usr/bin/yum install -y"
      /usr/bin/yum groupinstall -y "Development Tools"
   fi
   GIT_PKG="git"
   RUBY_PREREQ_PKGS="openssl-devel readline-devel zlib-devel gdbm-devel"
elif [ -x /usr/bin/zypper ]; then
   INSTALL_CMD="/usr/bin/zypper install -y"
   /usr/bin/zypper install -y --type pattern devel_basis
   GIT_PKG="git"
   RUBY_PREREQ_PKGS="libopenssl-devel readline-devel"
elif [ -x /usr/bin/apt-get ]; then
   #releasever=$(lsb_release -r | awk '{ print $2 }')
   /usr/bin/apt-get update
   INSTALL_CMD="/usr/bin/apt-get install -y"
   /usr/bin/apt-get install -y build-essential
   GIT_PKG="git"
   RUBY_PREREQ_PKGS="libssl-dev libreadline-dev zlib1g-dev libgdbm-dev"
elif [ -x /usr/bin/pacman ]; then
   /usr/bin/pacman -Syy
   INSTALL_CMD="/usr/bin/pacman -S --verbose --noconfirm"
   GIT_PKG="git"
   RUBY_PREREQ_PKGS="gdbm"
elif [ -x /usr/bin/emerge ]; then
   INSTALL_CMD="/usr/bin/emerge -v"
   rm -f /usr/portage/metadata/timestamp.x
   /usr/bin/emerge-webrsync --quiet
   #/bin/sed -i -e 's@USE="\(.*\)"@USE="\1 curl webdav"@' /etc/portage/make.conf
   /bin/sed -i -e 's/-curl/curl/' -e 's/-webdav/webdav/' /etc/portage/package.use/git
   GIT_PKG="git"
   RUBY_PREREQ_PKGS="gdbm"
elif [ -x /usr/sbin/pkg ]; then
   INSTALL_CMD="/usr/sbin/pkg install -y"
   GIT_PKG="git"
   RUBY_PREREQ_PKGS="bash gdbm"
elif [ -x /usr/pkg/bin/pkgin ]; then
   INSTALL_CMD="/usr/pkg/bin/pkgin -y install"
   $INSTALL_CMD mozilla-rootcerts
   if ! /usr/pkg/sbin/mozilla-rootcerts install; then
      /usr/pkg/sbin/mozilla-rootcerts rehash
   fi
   GIT_PKG="git"
   RUBY_PREREQ_PKGS="bash gdbm"
elif [ -x /usr/sbin/pkg_add ]; then
   #export PKG_PATH=http://mirrors.mit.edu/pub/OpenBSD/$(uname -r)/packages/$(machine -a)/
   export PKG_PATH=http://ftp4.usa.openbsd.org/pub/OpenBSD/$(uname -r)/packages/$(machine -a)/
   export MAKE=gmake
   export CPPFLAGS="-I/usr/local/include" 
   export LDFLAGS="-L/usr/local/lib"
   INSTALL_CMD="/usr/sbin/pkg_add -v"
   GIT_PKG="git"
   RUBY_PREREQ_PKGS="bash bzip2 gmake gdbm"
elif [ -x /usr/bin/pkg ]; then
   SOLFLAVOR=$( awk '{ print $1 ; exit}' /etc/release )
   if [ "${SOLFLAVOR}" = "OmniOS" ]; then
      /usr/bin/pkg set-publisher -g http://sfe.opencsw.org/localhostomnios localhostomnios
      INSTALL_CMD="/usr/bin/pkg install -v"
      GIT_PKG="/developer/versioning/git"
      RUBY_PREREQ_PKGS="/developer/gcc48 /developer/build/gnu-make /system/header /developer/object-file /library/database/gdbm"
      export CC=/opt/gcc-4.8.1/bin/gcc
      export MAKE=gmake
   elif [ "${SOLFLAVOR}" = "OpenIndiana" ]; then
      pkg set-publisher -O http://pkg.openindiana.org/dev openindiana.org 
      INSTALL_CMD="/usr/bin/pkg install -v"
      GIT_PKG=""  
      RUBY_PREREQ_PKGS=""
      export MAKE=/usr/gnu/bin/make
   elif [ "${SOLFLAVOR}" = "Oracle" ]; then   # Solaris 11
      INSTALL_CMD="/usr/bin/pkg install -v"
      GIT_PKG="/developer/versioning/git"
      RUBY_PREREQ_PKGS="/developer/gcc-48" 
      export MAKE=/usr/sfw/bin/gmake
   fi
fi   

if [ -z "$INSTALL_CMD" ]; then
   echo "No install command known for this OS. Exiting..."
   exit 1
fi
if [ ! -z "${GIT_PKG}" ]; then
   $INSTALL_CMD $GIT_PKG
fi
if [ -d ruby-build ]; then
   /bin/rm -rf ruby-build
fi
git clone $RUBY_BUILD_REPO
if [ ! -z "${RUBY_PREREQ_PKGS}" ]; then
   $INSTALL_CMD $RUBY_PREREQ_PKGS
fi
CONFIGURE_OPTS="--disable-install-doc --with-out-ext=tcl --with-out-ext=tk" RUBY_BUILD_BUILD_PATH="/var/tmp/ruby-build" ./ruby-build/bin/ruby-build -v $RUBY_VERSION /usr/local/ruby/$RUBY_VERSION
ln -s /usr/local/ruby/$RUBY_VERSION /usr/local/ruby/kitchen
