require 'minitest/autorun'
require 'minitest/spec'

require 'minitest-chef-handler'
#report_handlers << MiniTest::Chef::Handler.new

# http://engineering.aweber.com/test-driven-chef-cookbooks-with-test-kitchen/

describe_recipe 'example::default' do
     it "installed the git package" do
        # Needs minitest-chef-handler?
        # https://github.com/chef/minitest-chef-handler
        ## package("git").must_be_installed
	file('/etc/services').must_exist
  end
end
